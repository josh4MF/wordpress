# Wordpress - Clean Install #

A starting platform for creating a new Wordpress site.  The following features have been added to this initial build.

* tidythemes / blankslate
* elliotcondon / acf
* h5bp / html5-boilerplate
* ftlabs / fastclick
* Grunt
* LESS

The blankslate folder can be renamed to anything more suitable to the project, but will require the directory to be updated in Gruntfile.js, .gitignore, and the theme's functions.php file.

All CSS and JavaScript should be edited in the /dev folder.  Running grunt watch in terminal will automatically comple all of the CSS and JS into its dev format and will automatically refresh the page you are viewing.

When ready to push a commit live, run grunt production to minify all of the concatenated CSS and JS.  The theme's functions.php file will automatically place the minified version into the live commit.