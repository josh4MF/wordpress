module.exports = function(grunt) {

  grunt.initConfig({
    less: {
      compile: {
        options: {
          paths: ["dev/css"],
          plugins: [
            new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]})
          ]
        },
        files: {
          "dev/css/global.css": "dev/css/global.less"
        }
      }
    },
    clean: ["wp-content/themes/blankslate/style.dev.css", "wp-content/themes/blankslate/js/script.dev.js"],
    concat: {
      js: {
        src: ['dev/js/jquery/*.js','dev/js/plugins/*.js','dev/js/*.js'],
        dest: 'wp-content/themes/blankslate/js/script.dev.js'
      },
      css: {
        src: ['dev/css/*.css'],
        dest: 'wp-content/themes/blankslate/style.dev.css'
      }
    },
    uglify: {
      my_target: {
        files: {
          'wp-content/themes/blankslate/js/script.min.js': ['wp-content/themes/blankslate/js/script.dev.js']
        }
      }
    },
    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1,
      },
      target: {
        files: {
          'wp-content/themes/blankslate/style.min.css': ['wp-content/themes/blankslate/style.dev.css']
        }
      }
    },
    watch: {
      options: {
        livereload: true,
      },
      css: {
        files: ['dev/**/*', '!dev/css/global.css', 'wp-content/themes/blankslate/*.php'],
        tasks: ['less','clean','concat']
      },
    },
  });
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('production', ['less','clean','concat', 'cssmin', 'uglify']);
};